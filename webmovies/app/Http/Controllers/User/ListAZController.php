<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Movies;

class ListAZController extends Controller
{
    public function getListAZ(){
    	$movies =Movies::select('id','title','title_seo','director','thumb','category_id','country_id','actor_id','quality')->get();
    	return view('user.flist.list',compact('movies'));
    }
}
