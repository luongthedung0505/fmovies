@extends('user.master')
@section('contents')	
@include('user.blocks.social')
<!-- faq-banner -->
	<div class="faq">
		<h4 class="latest-text w3_faq_latest_text w3_latest_text">Movies List</h4>
		<div class="container">
			<div class="agileits-news-top">
				<ol class="breadcrumb">
				  <li><a href="{!! url('/')!!}">Home</a></li>
				  <li class="active">List</li>
				</ol>
			</div>
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">0 - 9</a></li>
					<li role="presentation"><a href="#a" role="tab" id="a-tab" data-toggle="tab" aria-controls="a">A</a></li>
					<li role="presentation"><a href="#b" role="tab" id="b-tab" data-toggle="tab" aria-controls="b">B</a></li>
					<li role="presentation"><a href="#c" role="tab" id="c-tab" data-toggle="tab" aria-controls="c">C</a></li>
					<li role="presentation"><a href="#d" role="tab" id="d-tab" data-toggle="tab" aria-controls="d">D</a></li>
					<li role="presentation"><a href="#e" role="tab" id="e-tab" data-toggle="tab" aria-controls="e">E</a></li>
					<li role="presentation"><a href="#f" role="tab" id="f-tab" data-toggle="tab" aria-controls="f">F</a></li>
					<li role="presentation"><a href="#g" role="tab" id="g-tab" data-toggle="tab" aria-controls="g">G</a></li>
					<li role="presentation"><a href="#h" role="tab" id="h-tab" data-toggle="tab" aria-controls="h">H</a></li>
					<li role="presentation"><a href="#i" role="tab" id="i-tab" data-toggle="tab" aria-controls="i">I</a></li>
					<li role="presentation"><a href="#j" role="tab" id="j-tab" data-toggle="tab" aria-controls="j">J</a></li>
					<li role="presentation"><a href="#k" role="tab" id="k-tab" data-toggle="tab" aria-controls="k">K</a></li>
					<li role="presentation"><a href="#l" role="tab" id="l-tab" data-toggle="tab" aria-controls="l">L</a></li>
					<li role="presentation"><a href="#m" role="tab" id="m-tab" data-toggle="tab" aria-controls="m">M</a></li>
					<li role="presentation"><a href="#n" role="tab" id="n-tab" data-toggle="tab" aria-controls="n">N</a></li>
					<li role="presentation"><a href="#o" role="tab" id="o-tab" data-toggle="tab" aria-controls="o">O</a></li>
					<li role="presentation"><a href="#p" role="tab" id="p-tab" data-toggle="tab" aria-controls="p">P</a></li>
					<li role="presentation"><a href="#q" role="tab" id="q-tab" data-toggle="tab" aria-controls="q">Q</a></li>
					<li role="presentation"><a href="#r" role="tab" id="r-tab" data-toggle="tab" aria-controls="r">R</a></li>
					<li role="presentation"><a href="#s" role="tab" id="s-tab" data-toggle="tab" aria-controls="s">S</a></li>
					<li role="presentation"><a href="#t" role="tab" id="t-tab" data-toggle="tab" aria-controls="t">T</a></li>
					<li role="presentation"><a href="#u" role="tab" id="u-tab" data-toggle="tab" aria-controls="u">U</a></li>
					<li role="presentation"><a href="#v" role="tab" id="v-tab" data-toggle="tab" aria-controls="v">V</a></li>
					<li role="presentation"><a href="#w" role="tab" id="w-tab" data-toggle="tab" aria-controls="w">W</a></li>
					<li role="presentation"><a href="#x" role="tab" id="x-tab" data-toggle="tab" aria-controls="x">X</a></li>
					<li role="presentation"><a href="#y" role="tab" id="y-tab" data-toggle="tab" aria-controls="y">Y</a></li>
					<li role="presentation"><a href="#z" role="tab" id="z-tab" data-toggle="tab" aria-controls="z">Z</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
						<div class="agile-news-table">
							<div class="w3ls-news-result">
								<h4>Search Results : <span>25</span></h4>
							</div>
							<table id="table-breakpoint">
								<thead>
								  	<tr>
										<th>No.</th>
										<th>Movie Name</th>
										<th>Status</th>
										<th>Country</th>
										<th>Genre</th>
										<th>Actor</th>
								  	</tr>
								</thead>
								<tbody>
									<?php $stt = 0 ?>
					              	@foreach($movies as $items)
					              	<?php $stt = $stt+1?>
								 	<tr>
										<td>{!!$stt!!}</td>
										<td class="w3-list-img">
											<a href="single.html">
												<img src="{!! asset('upload/movies/'.$items->thumb)!!}" alt="" /> 
												<span>{!! $items->title!!}</span>
											</a>
										</td>
										<td>{!! $items->quality!!}</td>
										<td class="w3-list-info">
											<a href="genres.html">
												<?php $country = DB::table('tb_country')->where('id',$items->country_id)->first();?>
							                    @if(!empty($country->name))
							                      {!! $country->name!!}
							                    @endif
						                    </a>
										</td>
										<td class="w3-list-info">
											<a href="comedy.html">
												<?php $cate = DB::table('tb_category')->where('id',$items->category_id)->first();?>
							                    @if(!empty($cate->name))
							                      {!! $cate->name!!}
							                    @endif
											</a>
										</td>
										<td>{!! $items->actor_id!!}</td>
								  	</tr>
								  	@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="a" aria-labelledby="a-tab">
						<div class="agile-news-table">
							<div class="w3ls-news-result">
								<h4>Search Results : <span>17</span></h4>
							</div>
							<table id="table-breakpoint">
								<thead>
								  	<tr>
										<th>No.</th>
										<th>Movie Name</th>
										<th>Status</th>
										<th>Country</th>
										<th>Genre</th>
										<th>Actor</th>
								  	</tr>
								</thead>
								<tbody>
									<?php $stt = 0 ?>
					              	@foreach($movies as $items)
					              	<?php $stt = $stt+1?>
								 	<tr>
										<td>{!!$stt!!}</td>
										<td class="w3-list-img">
											<a href="single.html">
												<img src="{!! asset('upload/movies/'.$items->thumb)!!}" alt="" /> 
												<span>{!! $items->title!!}</span>
											</a>
										</td>
										<td>{!! $items->quality!!}</td>
										<td class="w3-list-info">
											<a href="genres.html">
												<?php $country = DB::table('tb_country')->where('id',$items->country_id)->first();?>
							                    @if(!empty($country->name))
							                      {!! $country->name!!}
							                    @endif
						                    </a>
										</td>
										<td class="w3-list-info">
											<a href="comedy.html">
												<?php $cate = DB::table('tb_category')->where('id',$items->category_id)->first();?>
							                    @if(!empty($cate->name))
							                      {!! $cate->name!!}
							                    @endif
											</a>
										</td>
										<td>{!! $items->actor_id!!}</td>
								  	</tr>
								  	@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //faq-banner -->
@endsection